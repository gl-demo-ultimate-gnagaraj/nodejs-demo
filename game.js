class TicTacToeGame {
    constructor() {
        this.currentPlayer = 'X';
        this.board = Array(9).fill('');
        this.gameBoard = document.getElementById('gameBoard');
        this.createBoard();
    }

    createBoard() {
        for (let i = 0; i < 9; i++) {
            const cell = document.createElement('button');
            cell.className = 'cell';
            cell.addEventListener('click', () => this.makeMove(i));
            this.gameBoard.appendChild(cell);
        }
    }

    makeMove(index) {
        if (this.board[index] === '') {
            this.board[index] = this.currentPlayer;
            const cells = document.getElementsByClassName('cell');
            cells[index].textContent = this.currentPlayer;

            if (this.checkWinner()) {
                alert(`Player ${this.currentPlayer} wins!`);
                this.resetGame();
            } else if (!this.board.includes('')) {
                alert("It's a tie!");
                this.resetGame();
            } else {
                this.currentPlayer = this.currentPlayer === 'X' ? 'O' : 'X';
            }
        }
    }

    checkWinner() {
        const winCombinations = [
            [0, 1, 2], [3, 4, 5], [6, 7, 8], // rows
            [0, 3, 6], [1, 4, 7], [2, 5, 8], // columns
            [0, 4, 8], [2, 4, 6] // diagonals
        ];

        return winCombinations.some(combo => {
            return this.board[combo[0]] !== '' &&
                   this.board[combo[0]] === this.board[combo[1]] &&
                   this.board[combo[1]] === this.board[combo[2]];
        });
    }

    resetGame() {
        this.board = Array(9).fill('');
        this.currentPlayer = 'X';
        const cells = document.getElementsByClassName('cell');
        Array.from(cells).forEach(cell => cell.textContent = '');
    }
}

// Initialize the game when the page loads
window.onload = () => {
    new TicTacToeGame();
};